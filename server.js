var express = require('express');
var bodyParse = require('body-parser');
var path = require('path');


var app = express();
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));



app.get('/',function(req,res){
	res.send('success');
});

app.post('/test',function(req,res){
	console.log(req.body);
  res.json(req.body);
});



app.listen('3000', function(){
	console.log('listern port 3000');
});